package com.guide4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ValidationforminputApplication {

	public static void main(String[] args) {
		SpringApplication.run(ValidationforminputApplication.class, args);
	}

}
