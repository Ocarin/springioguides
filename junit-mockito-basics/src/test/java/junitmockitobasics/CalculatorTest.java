package junitmockitobasics;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Rule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.TestReporter;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.mockito.junit.MockitoRule;

import junitmockitobasics.model.CalculatorService;
import junitmockitobasics.model.CalculatorServiceImpl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.*;

//@RunWith(MockitoJUnitRunner.class)
class CalculatorTest {

	CalculatorServiceImpl calculator;
	
	//We create the Mock with Mockito. So now we don't have Error but Failure
	//If we haven't done a static import, then this is the line to use mock method from Mockito
	//CalculatorService calculatorService = Mockito.mock(CalculatorService.class);
	
	//If we have done a static import, then this is the line to use mock method from Mockito
	CalculatorService addService = mock(CalculatorService.class);
	
	//If we want to use Mockito with annotations. Then we need to use the following lines of code. The reason for these is because Mockito is a different framework which is being used in JUnit
	/*@Mock
	CalculatorService addService;
	
	@Rule
	public MockitoRule rule = MockitoJUnit.rule();
	*/
	
	TestInfo testInfo;
	TestReporter testReporter;
	private static final Logger Log = LoggerFactory.getLogger(CalculatorTest.class);
	
	@BeforeEach
	public void init(TestInfo testInfo, TestReporter testReporter)
	{
		this.testInfo = testInfo;
		this.testReporter = testReporter;
		calculator = new CalculatorServiceImpl(addService);
		
		Log.info("Entering at "+ testInfo.getDisplayName());
		System.out.println("Entering at "+ testInfo.getDisplayName());
		//This is the right way. By creating a report with the testReporter object which is specific for testing
		testReporter.publishEntry("Entering at "+ testInfo.getDisplayName()+" in class "+testInfo.getClass());
	}
	
	@AfterEach
	public void finalize()
	{
		Log.info("Existing from "+testInfo.getDisplayName());
		System.out.println("Existing from "+testInfo.getDisplayName());
		//This is the right way. By creating a report with the testReporter object which is specific for testing
		testReporter.publishEntry("Exiting at "+ testInfo.getDisplayName()+" in class "+testInfo.getClass());
	}
	
	@Test
	@DisplayName("should add two positive values")
	@Tag("Calculator")
	public void testPerformAdd() {
		System.out.println("In at "+ testInfo.getDisplayName()+" in class "+testInfo.getClass());
		//This is the right way. By creating a report with the testReporter object which is specific for testing
		testReporter.publishEntry("Exiting at "+ testInfo.getDisplayName()+" in class "+testInfo.getClass());
		int expected = 10;
		//We create the mock object with the values we decide
		when(addService.add(2, 3)).thenReturn(5);
		int actual = calculator.performAdd(2, 3);
		//We test our class
		assertEquals(expected,actual,"Adding two integer values");
		//We verify if our mock object was used. Verifying if we are actually calling our mock service
		verify(addService).add(2, 3);
	}

	@Test
	public void testPerformSubs() {
		System.out.println("In at "+ testInfo.getDisplayName()+" in class "+testInfo.getClass());
		//This is the right way. By creating a report with the testReporter object which is specific for testing
		testReporter.publishEntry("Exiting at "+ testInfo.getDisplayName()+" in class "+testInfo.getClass());
	}

	@Test
	@Disabled
	void testPerformMult() {
		fail("Not yet implemented");
		//This is the right way. By creating a report with the testReporter object which is specific for testing
		testReporter.publishEntry("Exiting at "+ testInfo.getDisplayName()+" in class "+testInfo.getClass());
	}

	@Test
	@Disabled
	void testPerformDiv() {
		fail("Not yet implemented");
		//This is the right way. By creating a report with the testReporter object which is specific for testing
		testReporter.publishEntry("Exiting at "+ testInfo.getDisplayName()+" in class "+testInfo.getClass());
	}

}
