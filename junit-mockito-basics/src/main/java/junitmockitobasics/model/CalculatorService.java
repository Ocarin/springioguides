package junitmockitobasics.model;

//Lets suppose this a web service
public interface CalculatorService {
	int add(int a, int b);
	int subs(int a, int b);
	int mult(int a, int b);
	double div(double a, double b);

}
