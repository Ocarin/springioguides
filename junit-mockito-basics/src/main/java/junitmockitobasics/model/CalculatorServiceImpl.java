package junitmockitobasics.model;

public class CalculatorServiceImpl{

	CalculatorService calculatorService;
	
	//Constructor Dependency Injection
	public CalculatorServiceImpl(CalculatorService calculatorService) {
		this.calculatorService = calculatorService;
	}
	
	//Setter Dependency Injection
	public void setCalculatorService(CalculatorService calculatorService)
	{
		this.calculatorService = calculatorService;
	}
	
	public int performAdd(int a, int b) {
		//return a+b;
		//Lets think calculatorService.add(a,b); as a web service from which we get a response.
		//Whatever result I get from the web service, I will multiply it by parameter a.
		//So I don't want to test the execution of the WS. I want to test the multiplication of a is correct.
		int resultFromWS = calculatorService.add(a, b);//I dont' want to test this.
		return resultFromWS*a;//I want to test this.
	}

	public int performSubs(int a, int b) {
		//return a-b;
		return calculatorService.subs(a, b);
	}

	public int performMult(int a, int b) {
		//return a*b;
		return calculatorService.mult(a, b);
	}

	public double performDiv(double a, double b) {
		//return a/b;
		return calculatorService.div(a, b);
	}
	
}
