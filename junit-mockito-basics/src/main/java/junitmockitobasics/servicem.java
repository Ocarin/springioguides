package junitmockitobasics;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class servicem {
	
	@RequestMapping(method = RequestMethod.GET, value="/hi")
	public String hi()
	{
		return "Hi";
	}
}
