package junitmockitobasics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JunitMockitoBasicsApplication {

	public static void main(String[] args) {
		SpringApplication.run(JunitMockitoBasicsApplication.class, args);
		 final Logger log = LoggerFactory.getLogger(JunitMockitoBasicsApplication.class);
		 log.info("In main method");
	}

}
