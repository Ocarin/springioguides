package com.guide5;

public class MathUtils {
	
	public int add(int a, int b)
	{
		return a+b;
	}
	
	public int substract(int a, int b)
	{
		return a-b;
	}
	
	public int multiply(int a, int b)
	{
		return a*b;
	}
	public double divide(int a, int b)
	{
		return a/b;
	}
	public double calculateArea(double radius)
	{
		return Math.PI * Math.pow(radius, 2);
	}
}
