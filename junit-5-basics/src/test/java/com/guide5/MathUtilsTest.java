package com.guide5;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestReporter;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;

//This is the default behavior.
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
//With this, only one class will be created for all the methods. So, you can have one instance who can be read from several methods. But, still, you don't have control over which method gets executed first. So, there is no guarantee that if a method is dependent of another, this work well. 
//@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DisplayName("When running MathUtils class")
class MathUtilsTest {

	MathUtils mathUtils;
	TestInfo testInfo;
	TestReporter testReporter;
	
	@BeforeAll
	@Tag("Config")
	static void beforeAllInit()
	{
		System.out.println("This method runs before All instances are created");
	}
	
	@AfterAll
	@Tag("Config")
	static void afterAllInit()
	{
		System.out.println("After All Init");
	}

	@BeforeEach
	@Tag("Config")
	void init(TestInfo testInfo, TestReporter testReporter)
	{
		this.testInfo = testInfo;
		this.testReporter = testReporter;
		testReporter.publishEntry("Running "+testInfo.getDisplayName()+" Class evaluated is "+testInfo.getClass()+ "with tags "+testInfo.getTags());
		mathUtils = new MathUtils();
	}
	@AfterEach
	@Tag("Config")
	void cleanUp()
	{
		System.out.println("Cleaning Up");
	}
	
	@Test
	//This annotation will display a nice name in the JUnit console. It is very handy.
	@DisplayName("Teste the add method with the expected value as 2")
	void testAdd1() {
		//MathUtils mathUtils = new MathUtils();
		
		int expected = 2;
		int actual = mathUtils.add(1, 1);
		assertEquals(expected, actual, "The add method should return the adition of two numbers");
		assertEquals(2,actual,"This is the actual method doing its job.");
		System.out.println("Dummy");
	}
	
	@Test
	@Tag("Math")
	void testAdd2()
	{
		//MathUtils mathUtils = new MathUtils();
		//Integer expected = null;
		//int actual = mathUtils.add(1,expected);
		int expected = 2;
		int actual = mathUtils.add(1,2);
		assertNotEquals(expected, actual);
	}

	@Test
	@Tag("Math")
	void testDivide1()
	{
		//MathUtils mathUtils = new MathUtils();
		//Tells that the second argument (of "executable" type) throws an exception of the type of the first parameter
		assertThrows(ArithmeticException.class, () -> mathUtils.divide(1,0),"Division by zero should throw");
		//assertThrows(NullPointerException.class, () -> mathUtils.divide(1,0),"Division by zero should throw");
	}
	
	@Test
	//This annotations is very handy. Lets say you are on TDD, and then you write down a little bit of functional code and then each test will always fail until you have the full source code. So, you are stuck and you won't be able to run your code. So, you can use @Disabled, to disable temporarily the functionality of the test. 
	@DisplayName("TDD. Method should not run!!")
	//@Disabled
	void testingDisable()
	{
		/*boolean serverIsUp = false;
		assumeTrue(serverIsUp);*/
		fail("This method will always fail");
	}
	
	@Test
	@DisplayName("Divide")
	void testDivide()
	{
		assertEquals(1,mathUtils.divide(1, 1),"Divide");
	}
	
	@Test
	@DisplayName("Test will run only if server is Up.")
	//Like @Disabled, these are called "Conditional execution"
	@EnabledOnOs(OS.WINDOWS)
	void testIfServerIsUp()
	{
		boolean isServerUp = false;
		//Assumptions are like @Disabled or @Enabled but, there difference is the we can control programatically if the test will be disabled or not. Assumptions are being made for external factors that mess with my source code, so the problem is not the source code, but an external factor, for example that the server is down. If the server is down, there is no need for my code to run the test case, because it will fail.
		//assumeTrue(boolean) skips the test if the assumption is not true.
		assumeTrue(isServerUp);
		assertTrue(isServerUp);
	}
	
	//Exercise on assrtAll
	@Test
	@DisplayName("Several Assertions are being run with the lambdas")
	void severalAdd()
	{
		assertAll(
				()->assertEquals(2,mathUtils.multiply(2, 1),"Multiplication of 2 into 1"),
				()->assertEquals(-2,mathUtils.multiply(2,-1),"Multiplication of 1 into -1"),
				()->assertEquals(6,mathUtils.multiply(2, 3),"Multiplication of 2 into 3")
				);
	}
	
	//Nested Tests with nested classes
	@Nested
	@DisplayName("substract method (test cases)")
	class substractTests
	{
		@Test
		@DisplayName("when substracting positive values")
		@Tag("Math")
		void testSubstract1()
		{
			assertEquals(2, mathUtils.substract(4, 2),"should return the right substraction");
		}
		@Test
		@DisplayName("when substracting negative values")
		@Tag("Math")
		void testSubstract2()
		{
			int expected = -2;
			int actual = mathUtils.substract(2, 4);
			//Lazy executions of asserts with lambdas
			//This will provide a costless fucntion. What it means is that the lambda function will only execute only when the test has failed and not everytime. This is because the message is concatenated everytime the test is being run and maybe this is very costly computationaly speaking
			assertEquals(expected,actual,()->"should return "+expected+" but returned "+actual);
		}
	}
	
	
	@Test
	@Tag("Calculations")
	@DisplayName("calculate area of integer values tests")
	@RepeatedTest(3)
	void testCalculateArea()
	{		
		double expected = Math.PI;
		double actual = mathUtils.calculateArea(1);
		assertEquals(expected, actual, "Should give the value of PI");
	}
}
