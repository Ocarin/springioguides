package com.guide5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecuringWeb1Application {

	public static void main(String[] args) {
		SpringApplication.run(SecuringWeb1Application.class, args);
		System.out.println("Application was run");
	}

}
