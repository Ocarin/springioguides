package com.guide5;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer{

	public void addViewControllers(ViewControllerRegistry registry)
	{
		registry.addViewController("/home").setViewName("myhome");
		registry.addViewController("/").setViewName("myhome");
		registry.addViewController("/hello").setViewName("hallo");
		registry.addViewController("/login").setViewName("login");
	}
}
