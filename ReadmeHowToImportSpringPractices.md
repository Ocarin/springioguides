# Instructions

## Step 1 Download STS
Download Spring Tool Suite IDE from the following link: [Download STS] (https://spring.io/tools)

Choose your computer operating system.

This will download a jar file of 375MB.

## Step 2 Install STS

Double clic the downloaded jar. The latter will unpack the file into a folder. Access the generated folder and double click the SpringToolSuite4.exe. The STS Launcher will pop up and ask you to select a directory as workspace.Please don't choose it yet.

This will give you access to the STS IDE. 

## Step 3 Get the project

Create a directory or folder in which you will like to run the projects. Only for convention put the word **workspace**. 

For example: c:/workspace


Open a git bash inside this folder and clone the project from GitLab or GitHub account. For example: 

$ git clone https://gitlab.com/Ocarin/springioguides.git

This will create a folder inside the workspace folder. 

## Step 4 Open the cloned project

In the STS Launcher, select the folder that was generated after the git cloning.

For example:

C:\workspace\springioguides

Once the STS is open, click menu File->Open Projects from File System...

In the import source text field paste the same path that was put in the STS Launcher (C:\workspace\springioguides).

The projects will be shown. Uncheck the base folder project.

## Run the projects
Right click on the project you will like to run and select Run As... Java Application or Run As... Spring Boot App

The application will run.

To see the results, open firefox and type in the address section. localhost:8080