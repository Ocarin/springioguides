package com.guides1.producerest.model;


public class Greeting {
	private final long id;
	private final String contentMessage;
	
	
	public Greeting(long id, String contentMessage) {
		super();
		this.id = id;
		this.contentMessage = contentMessage;
	}
	public long getId() {
		return id;
	}
	public String getContentMessage() {
		return contentMessage;
	}
	
	
}
