package com.guides1.producerest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.guides1.producerest.model","com.guides1.producerest.rest"})
public class ProducerestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProducerestApplication.class, args);
	}

}
